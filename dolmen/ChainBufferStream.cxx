#include "ChainBufferStream.h"

using namespace Dolmen;

using CSB = ChbStreamBuf;

CSB::int_type CSB::overflow(int_type ch)
{
    if (ch == traits_type::eof()) {
        return !syncOutput() ? traits_type::eof() : ch;
    }

    _wbuf.emplace_chunk(1, ch);
    return !syncIfOverflow() ? traits_type::eof() : ch;
}

std::streamsize CSB::xsputn(const char_type* s, std::streamsize n)
{
    _wbuf.emplace_chunk(s, s + n);
    syncIfOverflow();
    return n;
}

CSB::int_type CSB::underflow()
{
    syncIfUnderflow();
    return (_rbuf.empty()
            ? traits_type::eof()
            : traits_type::to_int_type(_rbuf.peek()));
}

CSB::int_type CSB::uflow()
{
    syncIfUnderflow();
    return (_rbuf.empty()
            ? traits_type::eof()
            : traits_type::to_int_type(_rbuf.shift()));
}

std::streamsize CSB::xsgetn (char* s, std::streamsize n)
{
    syncIfUnderflow();

    std::streamsize got = 0;

    while (got < n && !_rbuf.empty()) {
        auto& chunk = _rbuf.front();
        got += chunk.size();
        traits_type::copy(s + got, chunk.data(), chunk.size());
        _rbuf.pop_front();
    }

    return got;
}

bool CSB::syncIfOverflow()
{
    ssize_t overflow = _wbuf.size() - _wsize;
    return overflow > 0 ? syncOutput(overflow) : true;
}

bool CSB::syncIfUnderflow()
{
    ssize_t underflow = _rsize - _rbuf.size();
    return underflow > 0 ? syncInput(underflow) : true;
}

