#pragma once

#include "Socket.h"
#include "ChainBufferStream.h"

namespace Dolmen {

struct SocketStreamBuf final: public ChbStreamBuf
{
    using size_type = ChainBuffer::size_type;
    using value_type = ChainBuffer::value_type;

    ~SocketStreamBuf() { sync(); }

    SocketStreamBuf(Socket& sock)
        : ChbStreamBuf(_rbuf, _wbuf)
        , _sock(sock)
    { }

private:
    bool syncOutput(std::streamsize n) override;
    bool syncInput(std::streamsize n) override;

private:
    Socket& _sock;

    ChainBuffer _rbuf;
    ChainBuffer _wbuf;
};

struct SocketStream final: std::iostream
{
    SocketStream(Socket& sock): std::iostream(&_buf), _buf(sock) {}

private:
    SocketStreamBuf _buf;
};

}
