#include "EvConnection.h"

#include <algorithm>

#include "Socket.h"
#include "Utils.h"

using namespace Dolmen;
using namespace PD;

using EVC = EvConnection;
using EVCP = EvConnPool;


EVC::EvConnection(Socket* sock): _sock(sock)
{
    assert(sock);

    DLOG() << "C-r " << fd();
    _io.set<EvConnection, &EvConnection::cb_evIO>(this);
    _sock->setNonBlock();
}

EVC::~EvConnection()
{
    DLOG() << "~D-r " << fd();
    //stop();
}

void EVC::start(ev::loop_ref loop)
{
    if (_io.is_active()) return;

    if (fd() < 0)
        throw std::invalid_argument("Invalid fd");

    if (_io.is_active()) _io.stop();

    _io.set(loop);

    // TODO write support
    _io.start(fd(), ev::READ);

    DLOG() << "started " << fd();
}

void EVC::stop()
{
    if (!_io.is_active()) return;

    _io.stop();
    DLOG() << "stopped " << fd();
}

int EVC::fd() const { return _sock->fd(); }

void EVC::cb_evIO(ev::io &w, int re)
{
    if (ev::ERROR & re) {
        DLOG() << "got invalid event: " << strerror(errno);
        return;
    }

    if (ev::READ & re) { onRead(w.fd); }
    if (ev::WRITE & re) { onWrite(w.fd); }
}

void EVC::onRead(int fd)
{
    assert(fd == _sock->fd()); (void)fd;
    auto ret = _sock->read();
    if (!ret.first) stop();

    if (!ret.second.empty())
        readCb(this, std::move(ret.second));

    if (!ret.first && !!errorCb) errorCb(this);
}

void EVC::onWrite(int fd)
{
    assert(fd == _sock->fd()); (void)fd;
    // TODO
}


void EVCP::newConnection(EvConnection* conn)
{
    conn->errorCb = [this](IConnection* conn){connError(conn);};
    conn->start(loop);
    _conns.emplace(conn);
}

void EVCP::connError(IConnection* conn)
{
    DLOG() << "EvConnPool remove conn: " << conn->fd();
    _conns.erase(std::find_if(begin(_conns), end(_conns),
        [&conn](const ConnPtr& pn) {
            return pn.get() == conn;
        }));
}
