#pragma once

#include <vector>
#include <thread>
#include <algorithm>

#include "Utils.h"

namespace Dolmen {

using namespace PD;

template <class WorkerT>
struct ThreadPool final
{
    static const int DEFAULT_THREAD_CNT = 4;

    using WorkerThread = std::pair<std::thread, std::unique_ptr<IWorker>>;
    using WorkerSet = std::vector<WorkerThread>;

    ThreadPool(int thread_cnt = 0) {
        if (thread_cnt <= 0) {
            const auto hw_thread_cnt = std::thread::hardware_concurrency();
            thread_cnt = hw_thread_cnt
                ? hw_thread_cnt * 2
                : DEFAULT_THREAD_CNT;
        }

        LOG(INFO) << "Threads count: " << thread_cnt;

        for (int i = 0; i < thread_cnt; ++i) {
            auto wrk = new WorkerT{};

            _workers.emplace_back(
                std::piecewise_construct,
                std::forward_as_tuple(wrk->runInThread()),
                std::forward_as_tuple(std::unique_ptr<IWorker>(wrk))
            );
        }
    }

    ~ThreadPool() {
        for (auto&& w : _workers) {
            w.second->stop();
        }

        for (auto&& w : _workers) {
            w.first.join();
        }
    }

    template <class T>
    T* fitWorker() const {
        auto w = std::min_element(_workers.begin(), _workers.end(),
            [](const WorkerThread& w1, const WorkerThread& w2) {
                return dynamic_cast<T*>(w1.second.get())
                    && w1.second->busyness() < w2.second->busyness();
            });

        return w != _workers.end()
            ? dynamic_cast<T*>(w->second.get())
            : nullptr;
    }

private:
    WorkerSet _workers;
};

}
