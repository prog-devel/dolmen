#pragma once

#include <climits>
#include <cassert>

#include <vector>
#include <list>

#include <cstring> // memcpy

#include <iostream>

namespace Dolmen {

// TODO char_traits param
template <class CharT, class ContainerT = std::vector<CharT>>
struct BasicChainBuffer final
{
    using value_type = CharT;
    using cont_type = ContainerT;
    using size_type = typename cont_type::size_type;

    static const size_type MAX_SIZE = INT_MAX;

    struct DataChunk
    {
        using iterator = typename cont_type::iterator;
        using const_iterator = typename cont_type::const_iterator;

        template <class... Args>
        DataChunk(Args... args)
            : _cont(std::forward<Args>(args)...)
        {}

        inline const_iterator begin() const { return _cont.begin() + _offs; }
        inline const_iterator end() const { return _cont.end(); }

        inline size_type size() const
        {
            return _cont.size() - _offs;
        }

        inline bool empty() const { return size() == 0; }

        inline size_type base() const { return _offs; }

        inline value_type* data() { return _cont.data() + _offs; }
        inline const value_type* data() const { return _cont.data() + _offs; }

        void resize(size_type sz) { _cont.resize(_offs + sz); }

        inline value_type* shift(size_type noffs = 1)
        {
            auto ooffs = _offs;
            _offs += noffs;
            if (_offs > _cont.size()) _offs = _cont.size();
            return &*(_cont.begin() + ooffs);
        }

    private:
        cont_type _cont;
        size_type _offs{0};
    };

    using DataChain = std::list<DataChunk>;

    BasicChainBuffer drain(size_type n = MAX_SIZE, bool padded = false)
    {
        BasicChainBuffer outBuf;
        auto chainIt = _chain.begin();

        size_type rest = n;

        while (rest > 0 && chainIt != _chain.end())
        {
            auto& chunk = *chainIt;
            auto szchunk = chunk.size();

            if (rest < szchunk && !padded) break;

            rest -= szchunk;

            outBuf.emplace_chunk(std::move(chunk));
            chainIt = _chain.erase(chainIt);
        }

        if (rest > 0 && chainIt != _chain.end())
        {
            auto& chunk = *chainIt;

            outBuf.emplace_chunk(chunk.data(), chunk.data() + rest);

            chunk.shift(rest);
        }

        _size -= outBuf.size();
        return outBuf;
    }

    BasicChainBuffer& pullup(size_type n)
    {
        if (n > _size) n = _size;

        auto& outChunk = _chain.front();
        size_type pulluped = outChunk.size();

        if (_size == 0 || n <= pulluped)
            return *this;

        outChunk.resize(n);

        auto chainIt = ++_chain.begin();

        while (pulluped < n && chainIt != _chain.end())
        {
            auto& inChunk = *chainIt;
            auto ncpy = std::min(inChunk.size(), n - pulluped);

            // XXX only for PODs and SDT
            memcpy(outChunk.data() + pulluped,
                   inChunk.shift(ncpy),
                   sizeof(value_type) * ncpy);

            pulluped += ncpy;

            if (inChunk.empty())
                chainIt = _chain.erase(chainIt);
        }

        return *this;
    }

    // TODO operators +=, +
    BasicChainBuffer& concat(BasicChainBuffer buf) noexcept
    {
        _size += buf.size();
        _chain.splice(_chain.end(), buf._chain);
        return *this;
    }

    BasicChainBuffer& prepend(BasicChainBuffer buf) noexcept
    {
        _size += buf.size();
        _chain.splice(_chain.begin(), buf._chain);
        return *this;
    }

    template<class... Args>
    void emplace_chunk(Args... args)
    {
        DataChunk chunk(std::forward<Args>(args)...);
        _size += chunk.size();
        _chain.emplace_back(std::move(chunk));
    }

    inline const value_type& peek() const { return *_chain.front().data(); }

    value_type shift()
    {
        auto& chunk = *_chain.begin();
        auto val = std::move(*chunk.shift());
        if (chunk.empty()) _chain.pop_front();
        --_size;
        return val;
    }

    const DataChunk& front() const { return *_chain.begin(); }

    void pop_front()
    {
        _size -= _chain.front().size();
        _chain.pop_front();
    }

    inline bool empty() const { return _chain.empty(); }
    inline size_type size() const { return _size; }

private:
    DataChain _chain;
    size_type _size{0};
};

using ChainBuffer = BasicChainBuffer<char>;

}
