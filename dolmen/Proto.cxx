#include "Proto.h"
#include "Utils.h"

using namespace Dolmen;
using namespace PD;

Depacketizer::Packets Depacketizer::operator()(ChainBuffer chb)
{
    _buf.concat(std::move(chb));

    Packets packs;

    while (true) {
        if (!_meta) readHeader();
        // XXX not else if!
        if (!_meta) break;

        packs.emplace_back(readBody());
    }

    return packs;
}

void Depacketizer::readHeader()
{
    if (_buf.size() < sizeof(Packet)) return;
    auto data = _buf.pullup(sizeof(Packet)).front().data();

    auto from = reinterpret_cast<const Packet*>(data);
    if (from->magic != Packet::MAGIC ||
        from->version != Packet::VERSION)
    {
        throw std::runtime_error("Invalid packet format");
    }

    _meta = proto().packetMeta(*from);

    if (!_meta) {
        LOG(ERROR) << "Unknown packet type" << (int)from->type << " skipping...";
    }
}

Packet* Depacketizer::readBody()
{
    if (_buf.size() < _meta->size()) return nullptr;

    auto data = _buf.pullup(_meta->size()).front().data();
    auto from = reinterpret_cast<const Packet*>(data);

    Packet* pack = _meta->alloc();
    _meta->assign(pack, from);
    _buf.drain(_meta->size());

    _meta = nullptr;

    return pack;
}


ChainBuffer& Packetizer::operator()(const Packet& p)
{
    auto meta = proto().packetMeta(p);

    if(!meta)
        throw std::invalid_argument("Invalid packet type");

    auto data = reinterpret_cast<const char*>(&p);
    _buf.emplace_chunk(data, data + meta->size());
    return _buf;
}


