#pragma once

#include "generic.h"

#include <climits>
#include <fcntl.h> // fcntl

#include "IPAddress.h"

namespace Dolmen {

class Socket
{
public:
    virtual ~Socket() { }

    virtual void listen(const IPAddress&) { }
    virtual void connect(const IPAddress&) { }

    template <class... Args>
    inline void connect(Args... args)
    { connect(IPAddress(std::forward<Args>(args)...)); }

    template <class... Args>
    inline void listen(Args... args)
    { listen(IPAddress(std::forward<Args>(args)...)); }

    inline void setNonBlock()
    {
        fcntl(fd(), F_SETFL, fcntl(fd(), F_GETFL, 0) | O_NONBLOCK);
    }

    virtual int fd() const = 0;
    inline bool opened() const { return fd() > 0; }

    virtual std::pair<bool, ChainBuffer> read(ssize_t sz = -1) = 0;
    virtual bool write(ChainBuffer& from) = 0;
};

}
