// TODO ev standalone asyncs (like std::async)

#pragma once

#include "generic.h"

#include <ev++.h>

namespace Dolmen {

struct EvWorker: public IWorker
{
    using Tasks = std::list<Task>;

    EvWorker();
    EvWorker(EvWorker&&) = delete;

    inline ev::loop_ref loop() const { return _loop; }

    void stop() override;
    void run() override;
    Future async(TaskFn task) override;

private:
    void cb_evAsync(ev::async&, int) { doTasks(); }
    void doTasks();

private:
    ev::dynamic_loop _loop;
    ev::async _aw;
    Tasks _tasks;
    std::recursive_mutex _m;
    std::thread::id _tid;
};

}
