#include "IPAddress.h"

#include <netdb.h> // hostent
#include <arpa/inet.h> // inet_ntoa
#include <cstring>

// TODO
// - async support

using namespace Dolmen;

std::ostream& operator<<(std::ostream& o, const IPAddress& addr)
{
    return o << addr.formatted();
}


IPAddressResolver::~IPAddressResolver()
{
    if (_addrs)
        freeaddrinfo(_addrs);
}

IPAddressResolver::IPAddressResolver(const IPAddress& addr, int sockType)
{
    struct addrinfo hints;
    memset(&hints, 0, sizeof(hints));
    int rv;

    hints.ai_family = AF_INET; // use AF_INET6 to force IPv6
    hints.ai_socktype = sockType;

    // if node != nullptr then return sockaddr will be suitable for bind
    // else then the flag is ignored
    hints.ai_flags |= AI_PASSIVE;
    // use numeric port format
    hints.ai_flags |= AI_NUMERICSERV;

    if ((rv = getaddrinfo(
                addr.host.length() > 0 ? addr.host.c_str() : nullptr,
                addr.port > 0 ? std::to_string(addr.port).c_str() : nullptr,
                &hints,
                &_addrs)) != 0)
    {
        _lastError = gai_strerror(rv);
    }
}

