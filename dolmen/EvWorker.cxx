#include "EvWorker.h"

#include <list>

#include "Utils.h"

using namespace Dolmen;
using namespace PD;

EvWorker::EvWorker()
{
    _aw.set(_loop);
    _aw.set<EvWorker, &EvWorker::cb_evAsync>(this);
}

void EvWorker::stop()
{
    if (!_aw.is_active()) return;
    async([this](IWorker*) { _aw.loop.break_loop(); });
}

EvWorker::Future EvWorker::async(TaskFn task)
{
    if (_tid == std::this_thread::get_id() || !_aw.is_active()) {
        Task pt(task);
        pt(this);
        return pt.get_future();
    }

    std::unique_lock<std::recursive_mutex> lock(_m);

    DLOG() << "Send task to worker";
    _tasks.emplace_back(std::move(task));
    _aw.send();
    return _tasks.back().get_future();
}

void EvWorker::run()
{
    if (_aw.is_active()) return;
    LOG(INFO) << "Start worker ev-loop in thread: ";

    _tid = std::this_thread::get_id();

    _m.lock();
    _tasks.clear();
    _aw.start();
    _m.unlock();

    while (true)
    {
        std::exception_ptr ex;

        try {
            _loop.run(0);
        }
        catch (...) {
            ex = std::current_exception();
        }

        if (ex && !!errorCb) {
            errorCb(this, ex);
            // next step is RERUNNING of loop
        }
        else {
            if (ex) std::rethrow_exception(ex);
            break;
        }
    }

    _m.lock();
    _aw.stop();
    _m.unlock();

    LOG(INFO) << "Stop worker ev-loop ";
}

void EvWorker::doTasks()
{
    std::unique_lock<std::recursive_mutex> lock(_m);

    for (auto&& task : _tasks) {
        task(this);
    }
    _tasks.clear();
}
