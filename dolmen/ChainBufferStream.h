#pragma once

#include "ChainBuffer.h"

#include <climits>

namespace Dolmen {

struct ChbStreamBuf: public std::streambuf
{
    ChbStreamBuf(ChainBuffer& rbuf, ChainBuffer& wbuf)
        : _rbuf(rbuf)
        , _wbuf(wbuf)
    {}

    ChbStreamBuf(ChainBuffer& buf)
        : _rbuf(buf)
        , _wbuf(buf)
    {}

protected:
    virtual bool syncOutput(std::streamsize = INT_MAX) { return true; }
    virtual bool syncInput(std::streamsize = INT_MAX) { return !_rbuf.empty(); }

    int_type overflow(int_type ch) override;
    std::streamsize xsputn(const char_type* s, std::streamsize n) override;
    int_type underflow() override;
    int_type uflow() override;
    std::streamsize xsgetn (char* s, std::streamsize n) override;
    int sync() override { syncOutput(); return 0; }

    bool syncIfOverflow();
    bool syncIfUnderflow();

private:
    ChainBuffer& _rbuf;
    ChainBuffer& _wbuf;

    std::streamsize _wsize{1024};
    std::streamsize _rsize{1024};
};

struct ChainBufferStream final: public std::iostream
{
    ChainBufferStream(ChainBuffer& chb): std::iostream(&_buf), _buf(chb) {}

private:
    ChbStreamBuf _buf;
};

}
