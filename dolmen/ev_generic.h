#pragma once

#include <ev++.h>

namespace Dolmen
{

struct IEvActivity
{
    virtual ~IEvActivity() {}

    virtual void start(ev::loop_ref loop = ev::get_default_loop()) = 0;
    virtual void stop() = 0;
};

}

