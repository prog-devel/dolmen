#pragma once

#include <string>
#include <iostream>

struct addrinfo;

namespace Dolmen {

struct IPAddress
{
    IPAddress() = default;

    IPAddress(std::string hostName, uint16_t port)
        : host(std::move(hostName))
        , port(port)
    { }

    IPAddress(uint16_t port)
        : port(port)
    { }

    inline std::string formatted() const
    {
        return host + ":" + std::to_string(port);
    }

    std::string host;
    uint16_t port;
};

struct IPAddressResolver
{
    IPAddressResolver() = default;
    IPAddressResolver(const IPAddress& addr, int sockType);
    ~IPAddressResolver();

    inline struct addrinfo const* addrs() const { return _addrs; }
    inline std::string const& lastError() const { return _lastError; }

private:
    std::string _lastError;
    struct addrinfo* _addrs{nullptr};
};

}

std::ostream& operator<<(std::ostream& o, const Dolmen::IPAddress& addr);
