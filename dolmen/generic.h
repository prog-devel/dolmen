#pragma once

#include "utils.h"

#include <stdexcept>
#include <utility> // move, forward

#include <future>
#include <thread>

#include "ChainBuffer.h"

namespace Dolmen {

struct IRunnable
{
    virtual void run() = 0;

    void operator()() { run(); }

    std::thread runInThread() {
        // It's safe according to ref:
        // "The completion of the invocation of the constructor
        // synchronizes with the beginning of the invocation
        // of the copy of f."
        return std::thread(std::ref(*this));
    }
};

struct IWorker: public IRunnable, public IConvertible<IWorker>
{
    using RetType = void;
    using TaskFnSign = RetType(IWorker*);
    using TaskFn = std::function<TaskFnSign>;
    using Task = std::packaged_task<TaskFnSign>;
    using Future = std::future<RetType>;

    using ErrorCb = std::function<void(IWorker*, std::exception_ptr)>;

    ErrorCb errorCb;

    virtual ~IWorker() = default;

    virtual Future async(TaskFn) = 0;
    virtual void stop() = 0;

    virtual int busyness() const { return 0; }
};

struct IConnection
{
    using Cb = std::function<void(IConnection*)>;
    using ErrorCb = Cb;
    using ReadCb  = std::function<void(IConnection*, ChainBuffer data)>;
    using WriteCb = Cb;

    ErrorCb errorCb;
    ReadCb readCb;
    WriteCb writeCb;

    IConnection() = default;
    IConnection(const IConnection&) = delete;

    virtual ~IConnection() {}

    // TODO remove it
    virtual int fd() const { return -1; }

    virtual void onRead(int fd) = 0;
    virtual void onWrite(int fd) = 0;
};

}
