#include "TCPServer.h"

#include "EvConnection.h"

using namespace Dolmen;

TCPServer::TCPServer(const IPAddress& addr, ev::loop_ref loop)
{
    _sock.setNonBlock();
    _sock.listen(addr);

    _io.set<TCPServer, &TCPServer::cb_evIO>(this);
    start(loop);
}

void TCPServer::start(ev::loop_ref loop)
{
    if (_io.is_active()) return;
    _io.set(loop);
    _io.start(_sock.fd(), ev::READ);
}

void TCPServer::stop()
{
    if (!_io.is_active()) return;
    _io.stop();
}

void TCPServer::cb_evIO(ev::io&, int re)
{
    if (ev::ERROR & re) {
        perror("got invalid event");
        return;
    }

    auto csock = _sock.accept();

    if (!acceptor)
        delete csock;
    else
        acceptor(new EvConnection(csock));
}


