#include "TCPSocket.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h> // sockaddr
#include <arpa/inet.h> // inet_addr
#include <netdb.h> // addrinfo

#include <fcntl.h> // fcntl
#include <unistd.h> // close

#include <cassert>
#include <cerrno> // errno
#include <cstring> // strerror
#include <strings.h> // bzero

#include <string> // string, string_literals
#include <algorithm>

#include "Utils.h" // Logger

using namespace Dolmen;
using namespace Literals;
using namespace PD;

TCPSocket::TCPSocket(int fd) : _fd(fd)
{
    if (!opened()) {
        throw std::invalid_argument("Invalid socket fd");
    }
}

TCPSocket::~TCPSocket()
{
    close();
}

struct sockaddr* TCPSocket::open(const IPAddress& addr)
{
    if (opened())
        throw std::runtime_error{"Already opened"};

    auto ares = IPAddressResolver(addr, SOCK_STREAM);
    auto saddr = ares.addrs();
    if (!saddr)
        throw std::runtime_error{"Can't resolve "_s + addr.formatted() + " : " + ares.lastError()};

    _fd = socket(saddr->ai_family, saddr->ai_socktype, saddr->ai_protocol);
    if (_fd <= 0)
        throw std::runtime_error{"Can't create socket for "_s + addr.formatted() + " : " + strerror(errno)};

    static __thread sockaddr retaddr;
    memcpy(&retaddr, saddr->ai_addr, sizeof(retaddr));

    DLOG() << "open " << _fd;

    return &retaddr;
}

void TCPSocket::close()
{
    if (!opened()) return;
    DLOG() << "close " << _fd;
    ::close(_fd);
}

void TCPSocket::listen(const IPAddress& addr)
{
    auto saddr = open(addr);
    int reuse = 1;
    if (setsockopt(_fd, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(reuse)) != 0) {
        throw std::runtime_error{"setsockopt(SO_REUSEADDR) failed "_s + addr.formatted() + " : " + strerror(errno)};
    }

    if (::bind(_fd, saddr, sizeof(struct sockaddr)) != 0) {
        throw std::runtime_error{"Can't bind to "_s + addr.formatted() + " : " + strerror(errno)};
    }

    if (::listen(_fd, 5) != 0) {
        throw std::runtime_error{"Can't listen to "_s + addr.formatted() + " :" + strerror(errno)};
    }
}

void TCPSocket::connect(const IPAddress& addr)
{
    auto saddr = open(addr);
    //Connect to remote server
    if (::connect(_fd, saddr, sizeof(struct sockaddr)) < 0)
    {
        throw std::runtime_error{"Can't connect to "_s + addr.formatted() + " : " + strerror(errno)};
    }
}

TCPSocket* TCPSocket::accept()
{
    struct sockaddr_in client_addr;
    socklen_t client_len = sizeof(client_addr);

    int cfd = ::accept(_fd, (struct sockaddr *)&client_addr, &client_len);

    if (cfd < 0) {
        DLOG() << "accept error: " << strerror(errno);
        return nullptr;
    }

    return new TCPSocket(cfd);
}

std::pair<bool, ChainBuffer> TCPSocket::read(ssize_t sz)
{
    static __thread char buf[CHUNK_SZ];

    ChainBuffer chb;

    ssize_t nread = 0;

    while ((sz <= 0 || chb.size() < (size_t)sz) &&
           (nread = recv(_fd, buf, sizeof(buf), 0)) > 0)
    {
        chb.emplace_chunk(buf, buf + nread);
    }

    if (nread > 0 || (nread < 0 && errno == EAGAIN)) {
        return {true, chb};
    }

    DLOG() << "read error: " << strerror(errno);
    return {false, chb};
}

bool TCPSocket::write(ChainBuffer& from)
{
    if (from.empty()) return true;

    ssize_t nwrite = 0;

    while (!from.empty())
    {
        // TODO pullup filter ?
        from.pullup(CHUNK_SZ);
        auto chunk = from.front();
        nwrite = ::write(_fd, chunk.data(), chunk.size());
        if (nwrite <= 0) break;
        from.drain(nwrite);
    }

    if (nwrite > 0 || (nwrite < 0 && errno == EAGAIN)) {
        return true;
    }

    perror("write error");
    return false;
}


