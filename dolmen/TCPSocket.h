#pragma once

#include "Socket.h"

#include "IPAddress.h"

struct sockaddr;

namespace Dolmen
{

struct TCPSocket: public Socket
{
    static constexpr size_t CHUNK_SZ = 1024;

    TCPSocket(TCPSocket&& s): _fd(s._fd) { s._fd = -1;  }

    // TODO socket dup
    TCPSocket(const TCPSocket&) = delete;

    TCPSocket(int fd);
    TCPSocket() = default;
    ~TCPSocket();

    void listen(const IPAddress& addr) override;
    void connect(const IPAddress& addr) override;
    int fd() const override { return _fd; }

    using Socket::connect;
    using Socket::listen;

    std::pair<bool, ChainBuffer> read(ssize_t sz = -1) override;
    bool write(ChainBuffer& from) override;

    TCPSocket* accept();

private:
    struct sockaddr* open(const IPAddress& addr);
    void close();

private:
    int _fd{-1};
};

}
