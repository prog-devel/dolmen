#include "SocketStream.h"

using namespace Dolmen;
using SSB = SocketStreamBuf;

bool SSB::syncOutput(std::streamsize n)
{
    auto drained = _wbuf.drain(n);
    auto ret = _sock.write(drained);
    // unpush not sent data
    // TODO setup bad flag ?
    if (!ret) _wbuf.prepend(drained);
    return ret;
}

bool SSB::syncInput(std::streamsize n)
{
    auto ret = _sock.read(n);
    _rbuf.concat(std::move(ret.second));
    return ret.first;
}
