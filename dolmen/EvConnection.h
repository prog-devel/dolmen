#pragma once

#include "generic.h"
#include "ev_generic.h"

#include <set>

// TODO buffered connection

namespace Dolmen {

class Socket;

class EvConnection: public IConnection, public IEvActivity
{
public:
    EvConnection(Socket* sock);
    ~EvConnection();

    void start(ev::loop_ref loop) override;
    void stop() override;
    int fd() const override;

private:
    void cb_evIO(ev::io &w, int re);

    void onRead(int fd) override;
    void onWrite(int fd) override;

private:
    ev::io _io;
    std::unique_ptr<Socket> _sock;
};

class EvConnPool {
public:
    using ConnPtr = std::unique_ptr<EvConnection>;

    EvConnPool() = default;
    EvConnPool(ev::loop_ref loop): loop(loop) {}

    void newConnection(EvConnection* conn);

    inline int size() const { return _conns.size(); }

private:
    void connError(IConnection* conn);

private:
    std::set<ConnPtr> _conns;
    ev::loop_ref loop{ev::get_default_loop()};
};

}
