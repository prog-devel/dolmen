#pragma once

#include "generic.h"
#include "ev_generic.h"

#include "TCPSocket.h"

namespace Dolmen {

class EvConnection;

struct TCPServer final: public IEvActivity
{
    using ConnPtr = EvConnection*;
    using AcceptCb = std::function<void(ConnPtr)>;

    TCPServer(const IPAddress& addr,
              ev::loop_ref loop = ev::get_default_loop());

    ~TCPServer() { stop(); }

    AcceptCb acceptor;

private:
    void start(ev::loop_ref loop) override;
    void stop() override;
    void cb_evIO(ev::io& io, int re);

    TCPSocket _sock;
    ev::io _io;
};

}

