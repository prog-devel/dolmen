#pragma once

#include <functional>
#include <type_traits> // enable_if

#include <stdexcept>
#include <memory>
#include <string>

namespace Dolmen {

template<class T> class function;

// std::function hasn't non-const version of operator()
// needed by ev++
template<class R, class... Args>
class function<R(Args...)> {

    struct ICallable {
        virtual ~ICallable() = default;
        virtual R invoke(Args...) = 0;
    };

    template<class F>
    struct CallableObj final: public ICallable {
        CallableObj(F obj): _obj(std::move(obj)) {}

        R invoke(Args... args) override { return _obj(std::forward<Args>(args)...); }

    private:
        F _obj;
    };

public:
    template<class F>
    function(F fn): _cb(new CallableObj<F>(std::move(fn))) { }

    function() = default;

    inline function& operator=(std::nullptr_t) { _cb = nullptr; return *this; }

    inline R operator()(Args... args) const {
        return _cb->invoke(std::forward<Args>(args)...);
    }

    inline R operator()(Args... args) {
        return _cb->invoke(std::forward<Args>(args)...);
    }

    inline bool operator!() const { return !_cb; }

private:
    std::shared_ptr<ICallable> _cb;
};

template <class T>
struct Singleton {
    Singleton(Singleton&&) = delete;
    Singleton() = default;

    virtual ~Singleton() { }

    static T& inst() {
        if (!_inst) _inst.reset(new T);
        return *_inst.get();
    }

private:
    static std::unique_ptr<T> _inst;
};

template <class T>
std::unique_ptr<T> Singleton<T>::_inst;

template <class T>
struct IConvertible {
    virtual ~IConvertible() {}

    template <class To>
    typename std::enable_if<std::is_convertible<To*, T*>::value, To>
    ::type& as() {
        auto to = dynamic_cast<To*>(this);
        if (!to) throw std::runtime_error("Bad type cast");
        return *to;
    }

    template <class To>
    typename std::enable_if<std::is_convertible<T*, To*>::value, To>
    ::type& as() { return *static_cast<To*>(static_cast<T*>(this)); }
};

namespace Literals {

std::string operator"" _s(const char* cstr, size_t len);

}

}
