#pragma once

#include "ChainBuffer.h"

#include <map>
#include <memory>

namespace Dolmen {

#pragma pack(push, 1)
struct Packet
{
    using Type = uint32_t;
    enum Types { Empty = (Type)0, UserType };

    static constexpr uint32_t MAGIC = 0xDEADCAAB;
    static constexpr uint16_t VERSION = 1;

    uint32_t magic{MAGIC};
    uint16_t version{VERSION};
    Type type{Types::Empty};

    Packet(Type type): type(type) {}
};

#pragma pack(pop)

template <Packet::Type _type>
struct GenericTypedPacket: public Packet
{
    static const Type const_type;
    GenericTypedPacket(): Packet(_type) {}
};

template <Packet::Type _type>
const Packet::Type GenericTypedPacket<_type>::const_type = _type;

struct IPacketMeta
{
    using Type = Packet::Type;

    IPacketMeta(Type type): _type(type) { }

    virtual ~IPacketMeta() {}
    virtual size_t size() const = 0;
    Type type() const { return _type; }
    virtual Packet* alloc() const = 0;
    virtual void assign(Packet* to, const Packet* from) const = 0;

private:
    Type _type;
};

template <class T>
struct PacketMeta final: public IPacketMeta
{
    PacketMeta() : IPacketMeta(T::const_type) {}

    size_t size() const override { return sizeof(T); }
    Packet* alloc() const override { return new T; }

    void assign(Packet* to, const Packet* from) const override
    {
        *static_cast<T*>(to) = *static_cast<const T*>(from);
    }
};

template <class T>
T* packet_cast(Packet* p)
{
    return p && p->type == T::const_type ? static_cast<T*>(p) : nullptr;
}

struct BasicProto
{
    using PacketMetas = std::map<Packet::Type, IPacketMeta*>;
    using PacketMetaEntry = typename PacketMetas::value_type;

    BasicProto(const BasicProto&) = delete;
    BasicProto(PacketMetas metas): _metas(std::move(metas)) {}

    ~BasicProto() { for (auto&& pm : _metas) delete pm.second; }

    const IPacketMeta* packetMeta(const Packet& p) const
    {
        return _metas.count(p.type) > 0
            ? _metas.at(p.type)
            : nullptr;
    }

protected:
    template <class T>
    PacketMetaEntry newPacketMeta() const
    {
        return {
            T::const_type,
            new PacketMeta<T>
        };
    }

private:
    PacketMetas _metas;
};

template<class... PacketT>
struct Proto: public BasicProto
{
    Proto(): BasicProto{PacketMetas{newPacketMeta<PacketT>()...}} {}
};

struct PackDepack
{
    PackDepack(BasicProto& proto): _proto(proto) { }

    inline const BasicProto& proto() const { return _proto; }

private:
   BasicProto& _proto;
};

struct Depacketizer final: public PackDepack
{
    using Packets = std::list<std::unique_ptr<Packet>>;

    using PackDepack::PackDepack;

    Packets operator()(ChainBuffer chb);
    inline bool empty() const { return _buf.empty(); }

private:
    void readHeader();
    Packet* readBody();

private:
   ChainBuffer _buf;
   const IPacketMeta* _meta{nullptr};
};

struct Packetizer final: public PackDepack
{
    using PackDepack::PackDepack;

    ChainBuffer& operator()(const Packet& p);

private:
    ChainBuffer _buf;
};

}

