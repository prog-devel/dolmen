#include "CmdLineParser.h"

#include <iostream>
#include <sstream>
#include <iomanip>
#include <algorithm>

namespace PD {

//----------------------------------------------

CmdLineOptionInserter::CmdLineOptionInserter(const CmdLineOption& opt)
{
    CmdLineParser::shared().addOption(opt);
}

//----------------------------------------------

CmdLineParser::
ParseException::ParseException(const string &what, const string &where)
    : invalid_argument(what + ": \"" + where + "\"")
{
}

//----------------------------------------------

static CmdLineParser *shared_;


CmdLineParser& CmdLineParser::shared()
{
    if (!shared_)
        shared_ = new CmdLineParser();
    return *shared_;
}

CmdLineParser::CmdLineParser()
{
}

CmdLineParser::~CmdLineParser()
{
    if (shared_ == this)
        shared_ = nullptr;
}

void CmdLineParser::addOption(const CmdLineOption& opt)
{
    assert(!opt.shortName.empty() || !opt.longName.empty());

    opts_.push_back(opt);

    // optimization
    auto opt_ref = ref(opts_.back());

    // TODO check format
    if (!opt.longName.empty()) {
        longOpts_.emplace(opt.longName, opt_ref);
    }

    if (!opt.shortName.empty()) {
        assert(opt.shortName.length() == 1);
        shortOpts_.emplace(opt.shortName, opt_ref);
    }

    summary_ = "";
}

bool CmdLineParser::parse(int argc, const char *argv[], const Flags& flags) const
{
    assert(argc > 0 && argv);

    ParseOptRes res;

    try {
        while(--argc) {
            ++argv;
            switch (res.state) {
            case State::ParseOption: {
                res = parseOpt(*argv);
                break;
            }
            case State::ApplyOption:
                assert(res.opt);
                applyOpt(*res.opt, *argv);
                res.state = State::ParseOption;
                break;
            case State::Stop:
                break;
            default:
                assert(0);
            };
        }

        if (res.state != State::ParseOption && res.state != State::Stop) {
            throw ParseException("Incomplete parse", "");
        }
    }
    catch (const ParseException &pe) {
        lastError_ = pe.what();
        if (flags.test(Flag::ExceptOnError)) {
            throw;
        }
        else {
            DLOG() << lastError_;
            return false;
        }
    }
    catch (...) {
        throw;
    }

    return true;
}

CmdLineParser::ParseOptRes CmdLineParser::parseOpt(const string& arg) const
{
    DLOG() << "OPTION: " << arg;

    static const regex re(string("-(?:-([[:alnum:]]+|)(=(.*))?|([[:alnum:]].*))"),
                          regex::ECMAScript | regex::optimize);
    smatch match;

    if (regex_match(arg, match, re)) {
        assert(match.size() == 5);
        if (match[1].length() > 0) {
            parseLongOpt(match[1], match[3], match[2].length() > 0);
            return {State::ParseOption, nullptr};
        }
        else {
            if (!match[4].length()) {
                // -- : finish him!
                return {State::Stop, nullptr};
            }
            return parseShortOpt(match[4]);
        }
    }
    else {
        throw ParseException("Invalid arguments syntax", arg);
        return {};
    }
}

CmdLineParser::ParseOptRes CmdLineParser::parseShortOpt(const ssub_match& optRest) const
{
    // - short options могут быть сгруппированы,
    //   например: "-abcd", где a, b, c, d -- разные флаговые опции
    // - short option и её значение могут быть также сгруппированы,
    //   например: "-mfoo", где m -- параметризованная опция, foo -- значние

    for (auto optRestIt = optRest.first; optRestIt != optRest.second;) {
        string optName(optRestIt, optRestIt+1);

        DLOG() << "SHORT: " << optName;

        auto defOptIt = shortOpts_.find(optName);
        if (defOptIt == end(shortOpts_)) {
            throw ParseException("Unknown short option", optName);
        }


        const auto& defOpt = defOptIt->second.get();
        if (!defOpt.isFlag) {
            ++optRestIt;
            if (optRestIt == optRest.second) {
                // откладываем применение значения до следующего элемента в argv
                return {State::ApplyOption, &defOpt};
            }

            applyOpt(defOpt, string(optRestIt, optRest.second));
            optRestIt = optRest.second;
        }
        else {
            applyOpt(defOpt, "");
            ++optRestIt;
        }
    }

    return {State::ParseOption, nullptr};
}

void CmdLineParser::parseLongOpt(const ssub_match& optName,
                                 const ssub_match& val,
                                 bool hasAssign) const
{
    DLOG() << "LONG: " << optName;
    auto defOptIt = longOpts_.find(optName);
    if (defOptIt == end(longOpts_)) {
        throw ParseException("Unknown long option", optName);
    }

    const auto& defOpt = defOptIt->second.get();

    if (!defOpt.isFlag) {
        if (!hasAssign)
            throw ParseException("Option value expected", optName);
        applyOpt(defOpt, val);
    }
    else {
        if (hasAssign)
            throw ParseException("Unexpected option value", optName);
        applyOpt(defOpt, "");
    }
}

void CmdLineParser::applyOpt(const CmdLineOption& opt, const string& val) const
{
    if (!!opt.cb)
        opt.cb(val);
    else if (defCb_)
        defCb_(opt.longName, val, opt.isFlag);
}

const string& CmdLineParser::optionsSummary() const
{
    // cached
    if (!summary_.empty())
        return summary_;

    stringstream ss;
    string upName;

    for (const auto& opt : opts_) {
        stringstream fmt;

        if (!opt.shortName.empty()) {
            fmt << "-" << opt.shortName;

            if (!opt.longName.empty())
                fmt << ", ";
            else if (!opt.isFlag) {
                upName = "<ARG>";
                fmt << " " << upName;
            }
        }
        if (!opt.longName.empty()) {
            fmt << "--" << opt.longName;
            if (!opt.isFlag) {
                upName = "<" + opt.longName + ">";
                transform(upName.begin(),
                          upName.end(),
                          upName.begin(),
                          (int (*)(int))toupper);
                fmt << "=[" << upName << "]";
            }
        }

        ss << left << setw(30) << fmt.str();

        // interpolate descr template parameter '$$'
        auto trDescr = regex_replace(opt.descr, regex("\\$\\$"), upName);
        // multiline descr alignment
        bool firstLine = true;
        strSplit(trDescr, "\n", [&ss, &firstLine](const string& line) {
            if (!firstLine) ss << setw(30) << ' ';
            else firstLine = false;

            ss << setw(0) << line << endl;
        });
    }

    return (summary_ = ss.str());
}

}

