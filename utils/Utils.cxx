#include "Utils.h"

#include <strings.h>
#include <sys/types.h>
#include <unistd.h>

#include <thread>

namespace PD {

template<> bool& AnyTypeRef::as_ref() { return ref<bool, Type::Bool>(); }
template<> int& AnyTypeRef::as_ref() { return ref<int, Type::Int>(); }
template<> string& AnyTypeRef::as_ref() { return ref<string, Type::String>(); }

void AnyTypeRef::copy(const AnyTypeRef& var)
{
    type_ = var.type_;
    val_ = var.val_;
}

template<> string AnyTypeRef::as() const
{
    switch (type_) {
    case Type::String:
        return *val_.strVal;
    case Type::Int:
        return to_string(*val_.iVal);
    case Type::Bool:
        return *val_.bVal ? "true" : "false";
    default:
        assert(0);
        return "";
    }
}

AnyTypeRef& AnyTypeRef::operator = (bool val)
{
    switch (type_) {
    case Type::String:
        as_ref<string>() = val ? "true" : "false";
        break;
    case Type::Int:
        as_ref<int>() = (int)val;
        break;
    case Type::Bool:
        as_ref<bool>() = val;
        break;
    default:
        assert(0);
    }

    return *this;
}

AnyTypeRef& AnyTypeRef::operator = (int val)
{
    switch (type_) {
    case Type::String:
        as_ref<string>() = to_string(val);
        break;
    case Type::Int:
        as_ref<int>() = val;
        break;
    case Type::Bool:
        as_ref<bool>() = (bool)val;
        break;
    default:
        assert(0);
    }

    return *this;
}

AnyTypeRef& AnyTypeRef::operator = (const string& val)
{
    switch (type_) {
    case Type::String:
        as_ref<string>() = val;
        break;
    case Type::Int:
        as_ref<int>() = stoi(val);
        break;
    case Type::Bool: {
        const auto c_str = val.c_str();
        try {
            as_ref<bool>() = (   strcasecmp(c_str, "true") == 0
                              || strcasecmp(c_str, "yes") == 0
                              || strcasecmp(c_str, "on") == 0
                              || stoi(val) != 0);
        }
        catch (...) {
            as_ref<bool>() = false;
        }
        break;
    }
    default:
        assert(0);
    }

    return *this;
}


void strSplit(const string& str, const string& delim,
                  const function<void(const string&)>& inserter)
{
    size_t prev = 0, pos = 0;
    do
    {
        pos = str.find(delim, prev);
        if (pos == string::npos) pos = str.length();
        string token = str.substr(prev, pos-prev);
        if (!token.empty()) inserter(token);
        prev = pos + delim.length();
    }
    while (pos < str.length() && prev < str.length());
}

bool strEqual(const string& str1, const string& str2, bool icase)
{
    // TODO: oplimize it, localized
    return (!icase && str1 == str2) || (icase && 0 == strcasecmp(str1.c_str(), str2.c_str()));
}


ostream& Logger::of(char lvl)
{
    auto f = _of.flags();
    _of << lvl << getpid() << ":"
        << std::hex << std::this_thread::get_id() << " ";

    _of.flags(f);
    return _of;
}

}

