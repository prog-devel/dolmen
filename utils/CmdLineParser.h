/**
 * POSIX-совместимый парсер командной строки.
 *
 * TODO:
 *  - Возможность определять глобальные опции "на лету".
 *    Сейчас парсятся все опции за один раз, но существует возможность
 *    делать addOption в хэндлерах других опций, однако такая опция
 *    уже не будет глобальной и OPTION макрос работать не будет.
 *    Вероятно, такой подход не совсем верный и просто имеет смысл
 *    разбивать приложение на микро-тулзины, каждая со своим парсером опций.
 */

#pragma once

#include "Utils.h"

#include <cassert>

#include <stdexcept>
#include <functional>

#include <string>
#include <map>
#include <deque>

#if defined(__clang__) ||                      \
    (defined(__GNUC__) &&                      \
     (__GNUC__ > 4 ||                          \
      (__GNUC__ == 4 && (__GNUC_MINOR__ > 8))))

#include <regex> // on gcc 4.8 don't works properly

#else

#include <boost/regex.hpp>

namespace PD {
using boost::smatch;
using boost::ssub_match;
using boost::regex;
using boost::regex_match;
using boost::regex_replace;
}
#endif

namespace PD {

using namespace std;

/** \defgroup Объявление аргументов командной строки в декларативном стиле
 * @{
 */

/** Глобальная опция, привязанная к static переменной var типа type */
#define DEFINE_OPTION(type, var, deflt, longName, shortName, descr) \
    _DEFINE_OPTION(static type, var, AnyTypeRef, deflt, false, longName, shortName, descr)

/** Глобальная опция, привязанная к extern переменной var типа type */
#define DEFINE_EXT_OPTION(type, var, deflt, longName, shortName, descr) \
    _DEFINE_OPTION(type, var, AnyTypeRef, deflt, false, longName, shortName, descr)

/** Глобальная флаговая опция, привязанная к static переменной-счетчику var */
#define DEFINE_FLAG_OPTION(var, longName, shortName, descr) \
    _DEFINE_OPTION(static int, var, AnyTypeFlagRef, 0, true, longName, shortName, descr)

/** Глобальная флаговая опция, привязанная к extern переменной-счетчику var */
#define DEFINE_EXT_FLAG_OPTION(var, longName, shortName, descr) \
    _DEFINE_OPTION(int, var, AnyTypeFlagRef, 0, true, longName, shortName, descr)

/** Глобальная опция с обработчикм func типа function<void(const string& val)> */
#define DEFINE_OPTION_CB(func, longName, shortName, descr) \
    static auto PD_CMD_LINE__##__LINE__ ##opt = CmdLineOptionInserter(CmdLineOption{longName, shortName, false, func, descr});

/** Глобальная опция с обработчикм func типа function<void()> */
#define DEFINE_FLAG_OPTION_CB(func, longName, shortName, descr) \
    static auto PD_CMD_LINE__##__LINE__ ##opt = CmdLineOptionInserter(CmdLineOption{longName, shortName, true, [](const string&){func();}, descr});

/** Объявление extern-опции, определенной в другом юните */
#define DECLARE_EXT_OPTION(type, var) \
    extern type PD_CMD_LINE__##var;

/** Получение доступа к переменной, связанной с опцией */
#define OPTION(var) PD_CMD_LINE__##var
/**@}*/


/**
 * Описание опции для CmdLineParser.
 * (\sa CmdLineParser::addOption)
 */
struct CmdLineOption {
    typedef function<void(const string& val)> Cb;

    string longName;
    string shortName;
    bool isFlag; //< Опции-флаги не могут принимать значения
    Cb cb; //< Обработчик опции
    string descr;
};


/**
 * inserter реализует логику placement addOption для CmdLineParser,
 * что позволяет определять опции в static memory
 * в деклоративном стиле
 */
struct CmdLineOptionInserter {
    CmdLineOptionInserter(const CmdLineOption& opt);
};


/**
 * Ссылка на флаг.
 * Когда дёргается, как callable, то происходит инкремент.
 * Т.о. можно подсчитать, сколько раз был применен флаг в командной строке.
 */
struct AnyTypeFlagRef: AnyTypeRef {
    AnyTypeFlagRef(int &counter) : AnyTypeRef(counter) {}
    inline void operator () (const string&) { ++as_ref<int>(); }
};


class CmdLineParser {
public:
    enum class Flag : int {
        None          = 0x00,
        ExceptOnError = 0x01 << 0
    };

    typedef PD::Flags<Flag> Flags;

    typedef map<string, reference_wrapper<CmdLineOption> > OptionsMap;

    typedef function<void(const string& name, const string& val, bool isFlag)>
        DefaultCb;

    //----------------------------------------------

    static CmdLineParser& shared();
    CmdLineParser();
    virtual ~CmdLineParser();

    void addOption(const CmdLineOption& opt);
    const string& optionsSummary() const;

    /** Callback на случай, если не указан для опции */
    inline void setDefaultCb(const DefaultCb& cb) { defCb_ = cb; }

    /**
     * Парсит параметры командной строки (как они были переданы в main).
     *
     * Прежде чем дёргать этот метод, стоит добавить описание опций,
     * которые хотим парсить (\sa addOption)
     */
    virtual bool parse(int argc, const char *argv[], const Flags& flags = Flag::None) const;

    inline string lastError() const { return lastError_; }

private:
    enum class State : int {
        ParseOption,
        ApplyOption,
        Stop
    };

    struct ParseOptRes {
        State state{State::ParseOption};
        const CmdLineOption* opt{nullptr};
    };

    struct ParseException: public invalid_argument {
        ParseException(const string &what, const string &where);
    };

    ParseOptRes parseOpt(const string& arg) const;
    ParseOptRes parseShortOpt(const ssub_match& optRest) const;
    void parseLongOpt(const ssub_match& optName, const ssub_match& val, bool hasAssign) const;

    void applyOpt(const CmdLineOption& opt, const string& val) const;

private:
    deque<CmdLineOption> opts_;
    OptionsMap longOpts_;
    OptionsMap shortOpts_;
    DefaultCb defCb_;

    mutable string summary_;
    mutable string lastError_;
};


inline bool parseCmdLine(int argc, const char *argv[], const CmdLineParser::Flags& flags = CmdLineParser::Flag::None)
{
    return CmdLineParser::shared().parse(argc, argv, flags);
}

}

#define _DEFINE_OPTION(type, var, ref_type, deflt, flag, longName, shortName, descr) \
    type PD_CMD_LINE__##var = deflt; \
    static auto PD_CMD_LINE__##var ##opt = \
        CmdLineOptionInserter( \
            CmdLineOption{longName, shortName, flag, ref_type(PD_CMD_LINE__##var), descr} \
        );
