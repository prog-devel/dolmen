/**
 * То, чего не умеет stl, но ради чего не хочется тянуть сторонние либы
 * TODO:
 *  - min log level
 */
#pragma once

#include <unistd.h>
#include <cassert>
#include <cstring>

#include <type_traits>
#include <functional>

#include <iostream>

#include <string>

#ifndef NDEBUG
#define DLOG() Logger().of('D') << __LOG_LOCATION__
#else
#define DLOG() VOID_LOG()
#endif

#define LOG(level) LOG_ ## level()

namespace PD {

using namespace std;

void strSplit(const string& str, const string& delim,
              const function<void(const string&)>& inserter);

bool strEqual(const string& str1, const string& str2, bool icase = false);

template<class It>
struct EqPredForIt {
    typedef function<bool(const typename It::value_type&, const string&)> FType;
};

template<class It>
size_t countStrEq(It itBegin, It itEnd, const string& str,
                  const typename EqPredForIt<It>::FType& eqPred);


template <class EType>
class Flags final {
public:
    typedef typename std::underlying_type<EType>::type UType;

    Flags(EType f = static_cast<EType>(0)) : f_(f) {}
    Flags(const Flags<EType>& f) : f_(f.f_) {}

    inline Flags& operator = (EType f) { f_ = f; return *this; }
    inline Flags& operator = (const Flags<EType>& f) { f_ = f.f_; return *this; }

    inline Flags& operator |= (EType f) { set(f); return *this; }
    inline Flags operator | (EType f) const { return (Flags<EType>(f_) |= f); }

    inline operator EType () const { return f_; }

    inline bool test(EType f) const { return 0 != (static_cast<UType>(f_) & static_cast<UType>(f)); }
    inline void set(EType f) { f_ = (EType)(static_cast<UType>(f_) | static_cast<UType>(f)); }
    inline void unset(EType f) { f_ = (EType)(static_cast<UType>(f_) & ~static_cast<UType>(f)); }

private:
    EType f_;
};


/**
 * Черная магия: референс на условно любой тип
 * (пока только те, что нужны для CmdLineParser).
 * Умеет кастинг из одного в другое.
 * Умеет вести себя как callable.
 */
class AnyTypeRef {
public:
    enum class Type {
        Invalid,
        Bool,
        Int,
        String
    };

    //----------------------------------------------

    AnyTypeRef(nullptr_t) : type_(Type::Invalid) {}
    AnyTypeRef(const AnyTypeRef& var) { copy(var); }

    AnyTypeRef(bool& val)   : type_(Type::Bool), val_(val) {}
    AnyTypeRef(int& val)    : type_(Type::Int), val_(val) {}
    AnyTypeRef(string& val) : type_(Type::String), val_(val) {}

    ~AnyTypeRef() { }

    inline AnyTypeRef& operator = (const AnyTypeRef &var) { copy(var); return *this; }

    AnyTypeRef& operator = (bool val);
    AnyTypeRef& operator = (int val);
    AnyTypeRef& operator = (const string& val);
    inline AnyTypeRef& operator = (const char *val) { return operator=(string(val)); }

    // callable support
    template <class T>
    inline void operator () (const T& val) { operator = (val); }

    inline Type type() const { return type_; }
    inline operator bool() const { return type_ != Type::Invalid; }

    template <class T> T as() const;
    template <class T> T& as_ref();

private:
    void copy(const AnyTypeRef& var);

    template <class T, Type tag>
    T& ref() {
        assert(type_ != Type::Invalid && tag == type_);
        return *((T *)val_.val);
    }

private:
    Type type_;

    union UVal {
        UVal() : val(nullptr) {}
        UVal(bool& val) : bVal(&val) {}
        UVal(int& val) : iVal(&val) {}
        UVal(string& val) : strVal(&val) {}

        void* val;
        bool* bVal;
        int* iVal;
        string* strVal;
    } val_;
};


// TODO multi-thread sync buffer
struct Logger {
    Logger(ostream& of = cerr) : _of(of) {  }
    ~Logger() { _of << endl; }

    ostream& of(char lvl);

private:
    ostream& _of;
};


struct VoidLog {
    VoidLog() {  }
    void operator&&(ostream &) {  }
};

}

// Всё страшное (макросы, определение шаблонных функций) пускай полежит здесь
#include "Utils_p.h"
