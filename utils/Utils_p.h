#pragma once

// немного RAII-магии для переноса строк
#define LOG_ERROR() Logger().of('E') << __LOG_LOCATION__
#define LOG_INFO() Logger().of('I') << __LOG_LOCATION__

// немного void-магии и приоритетов операций не помешает
#define VOID_LOG() true ? (void)0 : VoidLog() && Logger().of('D')

#define __FILENAME__ ({                                             \
    static const auto f =                                           \
    strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__; \
    f;                                                              \
})

#define __LOG_LOCATION__ __FILENAME__ << ":" << __LINE__ << " ### "

namespace PD {

template<class It>
size_t countStrEq(It itBegin, It itEnd, const string& str,
                  const typename EqPredForIt<It>::FType& eqPred)
{
    return accumulate(itBegin, itEnd, 0, [&str,&eqPred](int acc, const typename It::value_type& m)->int {
        return eqPred(m, str) ? acc + 1 : acc;
    });
}

}
