# Dolmen
Simple ev-based threaded network framework.

![](/Stounhendj.jpg)

----------

## Requirements

> - linux only;
> - **clang ≥ 3.8** or **gcc ≥ 4.8** with C++14 std support;
> - CMake 3.1;
> - libev++;
> - boost-regex ≥ 1.54 (for old gcc versions).

## Building

```sh
mkdir build
cd build
cmake ..
make -j8
```

## Running
**On server side:**

```sh
./server -p2000
```

**On client side:**

```sh
./client --maxid=10 -t4 -N 50 -p2000
```

## Demo output
**Client:**

    I3537:7f307e61bb40 client.cxx:67 ### Remote server: localhost:2000
    I3537:7f307e61bb40 client.cxx:68 ### Max ID: 10
    I3537:7f307e61bb40 client.cxx:69 ### Threads: 4
    I3537:7f307e61bb40 client.cxx:70 ### Packets per thread: 50

**Server:**

    I3858:7f99a51f4b40 server.cxx:45 ### Server listening on :2000
    I3858:7f99a51f4b40 ThreadPool.h:29 ### Threads count: 8

    ============= DUMP ==============
    -2, 3, -5, -7, 1, -1, -2, 0, 2, -1, 4,
    =================================
    ============= DUMP ==============
    -5, 3, -2, 1, -1, 6, 11, -6, -4, 3, -2,
    =================================

## Usage

```sh
./client -h
./server -h
```
