#include "ServerApp.h"

#include <stdexcept>
#include <cassert>

#include "EvWorker.h"
#include "EvConnection.h"

#include "TCPServer.h"
#include "ThreadPool.h"
#include "Utils.h" // Logger

using namespace std::placeholders;

using namespace Dolmen;
using namespace PD;

struct Worker final: public EvWorker
{
    Worker(): _conns(loop()) {
        errorCb = ServerApp::inst().errorCb;
    }

    void newConnection(EvConnection* conn) {
        async([this, conn](IWorker*) {
            _conns.newConnection(conn);
        });
    }

    int busyness() const override {
        return _conns.size();
    }

private:
    using EvWorker::run;

    EvConnPool _conns;
};

struct Superviser final: public EvWorker
{
    ~Superviser() { stop(); }

    Superviser()
        : _server(ServerApp::inst().address, loop())
    {
        _sigInt.set(loop());
        _sigTerm.set(loop());

        _sigInt.set<Superviser, &Superviser::cb_evSig>(this);
        _sigTerm.set<Superviser, &Superviser::cb_evSig>(this);

        _sigInt.start(SIGINT);
        _sigTerm.start(SIGTERM);

        _server.acceptor = std::bind(&Superviser::cb_srvAccept, this, _1);
    }

    void run() override {
        _tpool.reset(new ThreadPool<Worker>(ServerApp::inst().threadCnt));

        EvWorker::run();
        LOG(INFO) << "Master ev-loop stopped";

        _tpool.reset(nullptr);
        LOG(INFO) << "Threads stopped";
    }

private:
    void cb_evSig(ev::sig&, int) {
        LOG(ERROR) << "Interrupt...";
        stop();
    };

    void cb_srvAccept(TCPServer::ConnPtr conn) {
        auto readCb = ServerApp::inst().readCb; // get cb by copy

        if (!readCb) {
            LOG(ERROR) << "No read callback for conn; delete it " << conn->fd();
            delete conn;
            return;
        }

        auto w = _tpool->fitWorker<Worker>();
        assert(w);

        conn->readCb = std::move(readCb);
        // pass connection to worker thread
        w->newConnection(conn);
    };

private:
    TCPServer _server;
    std::unique_ptr<ThreadPool<Worker>> _tpool;

    ev::sig _sigInt;
    ev::sig _sigTerm;
};

void ServerApp::run() { Superviser()(); }

template struct Dolmen::Singleton<ServerApp>;
