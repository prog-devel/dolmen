// TODO monitor threads states

#pragma once

#include "generic.h"
#include "IPAddress.h"

namespace Dolmen {

/**
 * Don't worry. It's dependency inversion
 */
struct ServerApp: public Singleton<ServerApp>, public IRunnable
{
    IPAddress address{"localhost", 2000};
    int threadCnt{0};

    IConnection::ReadCb readCb; /// threaded read callback
    IWorker::ErrorCb errorCb; /// threaded error handling

    void run() override;
};

extern template struct Singleton<ServerApp>;

}
