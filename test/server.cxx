#include "ServerApp.h"

//#include "ChainBufferStream.h"
#include "TestProto.h"
#include "CmdLineParser.h"
#include "Utils.h" // Logger

using namespace Dolmen;
using namespace Literals;
using namespace PD;

static void usage(bool exit = true);
static int error(const std::string& descr, bool usage = false);
static void justDoIt(Packet* p);

static std::mutex m;
static std::vector<int> table;
static TestProto proto;


DEFINE_OPTION(std::string, listen, "", "listen", "l",
              "Network address listen to");

DEFINE_OPTION(int, port, 2000, "port", "p",
              "Network port listen to");

DEFINE_OPTION(int, threadCnt, 0,
              "threads", "t", "Threads count");

DEFINE_FLAG_OPTION_CB(usage, "help", "h", "This page");



int main(int argc, const char *argv[])
{
    if (!parseCmdLine(argc, argv)) {
        return error("Invalid cmd line arguments", true);
    }

    auto&& app = ServerApp::inst();

    app.address = {OPTION(listen), (uint16_t)OPTION(port)};
    app.threadCnt = OPTION(threadCnt);

    LOG(INFO) << "Server listening on " << app.address;

    // threaded read function
    app.readCb = [&](IConnection*, ChainBuffer buf) {
        //ChainBufferStream io(buf);
        //std::cout << io.rdbuf();
        Depacketizer depack(proto);
        auto packs = depack(std::move(buf));
        for (auto&& pack : packs) {
            justDoIt(pack.get());
        }
    };

    // threaded error function
    app.errorCb = [&](IWorker*, std::exception_ptr ex) {
        try {
            std::rethrow_exception(ex);
        }
        catch (const std::exception& stdex) {
            LOG(ERROR) << "Exception: " << stdex.what();
        }
    };

    try {
        // run server application
        app();
    }
    catch (const std::exception& ex) {
        return error("Can't run ServerApp: "_s + ex.what());
    }

    return 0;
}



void usage(bool exit)
{
    std::cout << "Usage:\n\tserver [OPTIONS...]\nOptions:\n";
    std::cout << CmdLineParser::shared().optionsSummary();
    if (exit) ::exit(0);
}

int error(const std::string& descr, bool usage)
{
    LOG(ERROR) << "Error occured: " << descr;
    if (usage) ::usage(false);
    return -1;
}

static void doIncDec(IncDec* p)
{
    if (p->id > USHRT_MAX || std::abs(p->cmd) != 1) {
        LOG(ERROR) << "Invalid inc/dec id or command; skipping...";
        return;
    }

    std::unique_lock<std::mutex> lock;

    if (table.size() < p->id + 1UL)
        table.resize(p->id + 1UL);
    table[p->id] += p->cmd;
}

static void doDump()
{
    std::unique_lock<std::mutex> lock;
    std::cout << "============= DUMP ==============\n";

    std::copy(table.begin(), table.end(),
              std::ostream_iterator<int>(std::cout, ", "));

    std::cout << "\n=================================\n";

    table.clear();
}

void justDoIt(Packet* p)
{
    if (!p) return;

    if (auto incDec = packet_cast<IncDec>(p)) {
        doIncDec(incDec);
    }
    else if (packet_cast<CmdDump>(p)) {
        doDump();
    }
    else {
        LOG(ERROR) << "Unknown packet type " << (int)p->type << " skipping...";
    }
}


