#include "TCPSocket.h"

#include <iostream>
#include <ctime>
#include <chrono>

#include <future>

#include "SocketStream.h"
#include "TestProto.h"

#include "CmdLineParser.h"

using namespace Dolmen;
using namespace PD;
//using namespace std::chrono_literals;

static void usage(bool exit = true);
static int error(const std::string& descr, bool usage = false);
static int worker(TCPSocket sock);
static void master();

DEFINE_OPTION(std::string, host, "localhost", "host", "h",
              "Address of server connect to");

DEFINE_OPTION(int, port, 2000, "port", "p",
              "Port of server connect to");

DEFINE_OPTION(int, maxId, 64, "maxid", "",
              "Max ID value in query");

DEFINE_OPTION(int, threadCnt, std::thread::hardware_concurrency(),
              "threads", "t", "Threads count");

DEFINE_OPTION(int, packetsCnt, 10,
              "packets", "N", "Packets per thread");

DEFINE_FLAG_OPTION_CB(usage, "help", "", "This page");

struct Conf: public Singleton<Conf>
{
    int maxId;
    int threadCnt;
    int packetsCnt;
    IPAddress addr;
};

static TestProto proto;


int main(int argc, const char *argv[])
{
    if (!parseCmdLine(argc, argv)) {
        return error("Invalid cmd line arguments", true);
    }

    srand(time(nullptr));

    auto& conf = Conf::inst();

    conf.addr = {OPTION(host), (uint16_t)OPTION(port)};
    conf.maxId = OPTION(maxId);
    conf.threadCnt = OPTION(threadCnt);
    conf.packetsCnt = OPTION(packetsCnt);

    // TODO config dump function ?
    LOG(INFO) << "Remote server: " << conf.addr;
    LOG(INFO) << "Max ID: " << conf.maxId;
    LOG(INFO) << "Threads: " << conf.threadCnt;
    LOG(INFO) << "Packets per thread: " << conf.packetsCnt;

    try {
        master();
    }
    catch (const std::exception& ex) {
        LOG(ERROR) << "Error occured: " << ex.what();
        return -1;
    }

    return 0;
}



void usage(bool exit)
{
    std::cout << "Usage:\n\tclient [OPTIONS...]\nOptions:\n";
    std::cout << CmdLineParser::shared().optionsSummary();
    if (exit) ::exit(0);
}

int error(const std::string& descr, bool usage)
{
    LOG(ERROR) << "Error occured: " << descr;
    if (usage) ::usage(false);
    return -1;
}

int worker(TCPSocket sock)
{
    const auto& conf = Conf::inst();

    Packetizer pack(proto);

    // TODO param
    for (int i = 0; i < conf.packetsCnt; ++i) {
        IncDec packet{
            (uint16_t)(rand() % (conf.maxId + 1)),
            (int8_t)(rand() % 2 ? -1 : 1)
        };

        //std::cout << "### packet: " << packet.id << " " << (int)packet.cmd << '\n';

        sock.write(pack(packet));
    }

    //std::this_thread::sleep_for(2s);
    return 0;
}

void master()
{
    const auto& conf = Conf::inst();

    TCPSocket sock{};
    sock.connect(conf.addr);

    Packetizer pack(proto);

    std::vector<std::future<int>> tasks;

    for (int i = 0; i < conf.threadCnt; ++i) {
        TCPSocket sock{};
        sock.connect(conf.addr);

        tasks.emplace_back(std::async(worker, std::move(sock)));
    }

    // `get' re-throws exceptions from threads if any
    for (auto&& t : tasks) { t.get(); }

    sock.write(pack(CmdDump{}));
}
