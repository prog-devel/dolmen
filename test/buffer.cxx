#include "ChainBufferStream.h"

using namespace Dolmen;

int main(int argc, const char *argv[])
{
    ChainBuffer buf;
    ChainBufferStream io(buf);

    std::string str;

    std::cout << "### Before all\n";
    std::cout << "EOF: " << io.eof() << "\n";
    std::cout << "GOOD: " << io.good() << "\n";

    std::cout << "### After formatted write\n";
    io << "TEST" << 5;
    std::cout << "EOF: " << io.eof() << "\n";
    std::cout << "GOOD: " << io.good() << "\n";

    std::cout << "### After buff read\n";
    std::cout << io.rdbuf() << "\n";
    std::cout << "EOF: " << io.eof() << "\n";
    std::cout << "GOOD: " << io.good() << "\n";

    std::cout << "### After formatted lines write\n";
    io << "TEST2" << 6 << "\n" << "TEST2" << 6;
    std::cout << "EOF: " << io.eof() << "\n";
    std::cout << "GOOD: " << io.good() << "\n";

    std::cout << "### After lines read\n";
    io >> str;
    std::cout << str << "\n";
    io >> str;
    std::cout << str << "\n";
    std::cout << "EOF: " << io.eof() << "\n";
    std::cout << "GOOD: " << io.good() << "\n";

    std::cout << "### After block write\n";
    //io << str;
    io.write(str.data(), str.size());
    std::cout << "EOF: " << io.eof() << "\n";
    std::cout << "GOOD: " << io.good() << "\n";

    std::cout << "### After line read\n";
    io >> str;
    std::cout << str << "\n";

    return 0;
}
