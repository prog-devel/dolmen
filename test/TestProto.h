#pragma once

#include "Proto.h"

namespace dl = Dolmen;

#pragma pack(push, 1)
struct IncDec:
    public dl::GenericTypedPacket<dl::Packet::Types::UserType>
{
    uint16_t id; /// 0...MAX_ID
    int8_t cmd; /// -1 or 1

    IncDec() = default;

    IncDec(uint16_t id, int8_t cmd)
        : id(id)
        , cmd(cmd)
    { }
};

struct CmdDump:
    public dl::GenericTypedPacket<dl::Packet::Types::UserType + 1>
{
};

#pragma pack(pop)

using TestProto = dl::Proto<IncDec, CmdDump>;
